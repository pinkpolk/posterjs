# Poster.js

> JavaScript 结合`qrcodejs`生成商品海报.

## 主要

```text
dist/
├── poster.css
├── poster.js
└── generatePoster.js  (备选版本)
```

## 依赖

```
lib/
├── qrcode/qrcode.js       (qrcodejs)
└── qrcode/qrcode.min.js   (qrcodejs,compressed)
cdn
    /jquery/3.4.1/jquery.min.js
```

[davidshimjs/qrcodejs](https://github.com/davidshimjs/qrcodejs)

## 开始

### 安装

在浏览器:

```html
<link  href="/path/to/poster.css" rel="stylesheet">
<script src="/path/to/qrcode.js"></script>
<script src="/path/to/poster.js"></script>
```

### 用法

```javascript
const Poster = $.Poster.init(setting);// 初始化
$(document).on('click', '.btn', function () {// 给按钮添加点击事件
    Poster.drawPoster();// 生成海报
});
```

## 参数

### setting

|参数名称|必填|说明|
|-|-|-|
|bgImgUrl|是|海报背景图地址|
|qrCodeUrl|是|二维码地址|
|imgUrl|是|商品图片地址|
|title|是|商品标题|
|price|是|商品销售价格|
|mktPrice|否|商品市场价格|
|validText|否|商品有效期文本|

## 海报背景图

大小：`750px*1066px`

模版: `/imgs/商品海报模版.png`

<img src="./imgs/商品海报模版.png" width ="30%" height="30%" title="海报背景图模版" />

## 效果图

![效果图1](./imgs/效果图1.png)
![效果图2](./imgs/效果图2.png)


## License

[MIT](https://opensource.org/licenses/MIT) © [Peak Xin](https://xinyufeng.net/)