/**
 * jQuery PosterGenerator v1.0.0
 * https://gitee.com/peakxin/posterjs
 * 
 * Copyright 2019-2020, Peak Xin
 * Licensed under the MIT license.
 */
;
'use strict';

$('.generate-poster').on('click',function(){
    if($('.poster-box').length == 0)
    {
        var MakerPosterBGImage = 'http://tvplaza.oss-cn-qingdao.aliyuncs.com/images/9b/c9/da/aef2f3ba89d3c2d2651e295012d731cce6bcbc04.png';
        var html = '<div class="poster-box" style="display: none;"><div class="poster-img"><img id="poster" src="'+MakerPosterBGImage+'" alt=""><div class="close" ><span>&times;</span></div></div><div class="poster-bottom"><button type="button" class="download" >长按保存海报</button></div></div>';
        $('body').append(html);

        var setting = {
            id: 'poster',
            bgimg: MakerPosterBGImage,
            img: $('#goodsImages img:first').prop('src'),
            title: $('.goods-title').text().trim(),
            price: $('.goods-price').text().trim(),
            mkt_price: $('.goods-mkt-price').text().trim(),
            valid: $('.goods-valid').text().trim()
        };
        //console.log(setting);
        generatePoster(setting);
    }

    $('.poster-box').show();

    $(document).on('click', '.poster-box', function (e) {
        if (e.target.id != 'poster'){
            $('.poster-box').hide();
        }
    });

    $('.poster-box .download').on('click', function () {
        //downloadPoster($('#'+setting.id).prop('src'), setting.title+'海报');
    });
});


var generatePoster = function (setting){//背景图片，上面图片，文字

    var conf = {
        //bt:顶部边距,bl:左侧边距,br,右侧边距,w:海报宽度,h:海报高度
        poster: {bt: 100, bl: 30, br: 30, w: 750, h: 1066},
        goods: {
            image: {width: 690, height: 690, bottom: 20},
            title: {
                text: setting.title,
                font: {size: 32, weight: 'bold', family: 'Microsoft Yahei', color: '#333333', max_w: 410},
            },
            price: {
                text: setting.price,
                font: {size: 40, weight: 'bold', family: 'Microsoft Yahei', color: '#dc1c1c'},
            },
            mkt_price: {
                text: setting.mkt_price,
                font: {size: 25, weight: 'normal', family: 'Microsoft Yahei', color: '#979797'},
            },
            valid: {
                text: setting.valid,
                font: {size: 25, weight: 'normal', family: 'Microsoft Yahei', color: '#999999'},
            }
        },
        qrcode: {
            width: 180,height: 180, desc: '长按识别二维码', desctop: 20
        },
    };

    // 主图参数
    var goodsImgParams = {
        x: conf.poster.bl,
        y: conf.poster.bt,
        w: conf.goods.image.width,
        h: conf.goods.image.height
    };
    // 二维码参数
    var qrcodeParams = {
        x: conf.poster.w - conf.qrcode.width - conf.poster.br,
        y: goodsImgParams.y + goodsImgParams.h + conf.goods.image.bottom,
        w: conf.qrcode.width,
        h: conf.qrcode.height
    };
    // 二维码底部文字参数
    var qrcodeTextParams = {
        x: qrcodeParams.x + (qrcodeParams.w / 2),
        y: qrcodeParams.y + qrcodeParams.h + conf.qrcode.desctop * 2
    };
    // 标题参数
    var goodsTitleParams = {
        t: conf.goods.title.text,
        x: conf.poster.bl,
        y: goodsImgParams.y + goodsImgParams.h + conf.goods.image.bottom - 15,
        max_w: conf.goods.title.font.max_w,
        s: conf.goods.title.font.size
    };
    // 售价参数
    var goodsPriceParams = {
        t: conf.goods.price.text,
        x: conf.poster.bl,
        y: qrcodeParams.y + qrcodeParams.h,
    };
    if (conf.goods.mkt_price.text != undefined)
    {
        // 原价参数
        var goodsMKTPriceParams = {
            t: conf.goods.mkt_price.text,
            x: goodsPriceParams.x + 230,
            y: qrcodeParams.y + qrcodeParams.h,
        };
        // 原价删除线参数
        var goodsMKTPriceDelParams = {
            s: {
                x: goodsMKTPriceParams.x,
                y: goodsMKTPriceParams.y - (conf.goods.mkt_price.font.size*0.3),
            },
            e: {
                x: goodsMKTPriceParams.x + conf.goods.mkt_price.text.length*15,
                y: goodsMKTPriceParams.y - (conf.goods.mkt_price.font.size*0.3),
            },
        };
    }

    // 有效期参数
    var goodsValidParams = {
        t: conf.goods.valid.text,
        x: conf.poster.bl,
        y: goodsTitleParams.y + 136,
    };

    /*二维码*/
    var qrcodeImg = document.getElementById("posterQRCode").lastChild;

    /*canvas*/
    var canvas = document.createElement("canvas");
    canvas.width = conf.poster.w;
    canvas.height = conf.poster.h;
    var ctx = canvas.getContext("2d");

    var bgImg = new Image();// 背景图
    bgImg.crossOrigin = "anonymous";
    bgImg.src = setting.bgimg;
    bgImg.onload = function(){
        ctx.drawImage(bgImg , 0 , 0 , canvas.width , canvas.height);
        try{
            canvas.toDataURL("image/png")
        }catch (err){
            console.error('生成海报错误：绘制背景图失败(' + err + ')');
            return;
        }

        /*标题*/
        if (goodsTitleParams.t != undefined)
        {
            ctx.fillStyle = conf.goods.title.font.color;
            ctx.font = conf.goods.title.font.weight + ' ' + conf.goods.title.font.size + 'px ' + conf.goods.title.font.family;
            //ctx.textBaseline = "bottom";
            drawText(ctx, goodsTitleParams.t, goodsTitleParams.x, goodsTitleParams.y, goodsTitleParams.max_w, goodsTitleParams.s);
        }

        /*商品价格*/
        if (goodsPriceParams.t != undefined)
        {
            ctx.fillStyle = conf.goods.price.font.color;
            ctx.font = conf.goods.price.font.weight + ' ' + conf.goods.price.font.size + 'px ' + conf.goods.price.font.family;
            ctx.fillText(goodsPriceParams.t, goodsPriceParams.x, goodsPriceParams.y);
        }

        /*原价价格*/
        if (conf.goods.mkt_price.text != undefined)
        {
            ctx.fillStyle = conf.goods.mkt_price.font.color;
            ctx.font = conf.goods.mkt_price.font.weight + ' ' + conf.goods.mkt_price.font.size + 'px ' + conf.goods.mkt_price.font.family;
            ctx.fillText(conf.goods.mkt_price.text, goodsMKTPriceParams.x, goodsMKTPriceParams.y);
            /*删除线*/
            ctx.moveTo (goodsMKTPriceDelParams.s.x, goodsMKTPriceDelParams.s.y);// 设置起点状态
            ctx.lineTo (goodsMKTPriceDelParams.e.x, goodsMKTPriceDelParams.e.y);// 设置末端状态
            ctx.lineWidth = 2;// 设置线宽状态
            ctx.strokeStyle = conf.goods.mkt_price.font.color ;// 设置线的颜色状态
            ctx.stroke();// 进行绘制
        }

        /*商品有效期*/
        if (goodsValidParams.t != undefined)
        {
            ctx.fillStyle = conf.goods.valid.font.color;
            ctx.font = conf.goods.valid.font.weight + ' ' + conf.goods.valid.font.size + 'px ' + conf.goods.valid.font.family;
            ctx.fillText(goodsValidParams.t, goodsValidParams.x, goodsValidParams.y);
        }

        /*二维码和底部文字*/
        ctx.fillStyle = "#2a2a2a";
        ctx.font = "24px Microsoft Yahei";
        //ctx.textBaseline = "top";
        ctx.textAlign="center";
        ctx.fillText('长按识别二维码', qrcodeTextParams.x, qrcodeTextParams.y);
        ctx.drawImage(qrcodeImg, qrcodeParams.x, qrcodeParams.y, qrcodeImg.width, qrcodeImg.height);

        /*商品主图*/
        var goodsImage = new Image();
        goodsImage.crossOrigin = "anonymous";
        goodsImage.src = setting.img;
        goodsImage.onload = function(){
            ctx.drawImage(goodsImage , goodsImgParams.x , goodsImgParams.y , goodsImgParams.w , goodsImgParams.h);

            try{
                document.getElementById('poster').setAttribute('src' , canvas.toDataURL("image/png"));
            }catch (err){
                console.error('生成海报错误：绘制商品图失败(' + err + ')');
                return;
            }
        }

    }
}

/**
 * 画文字
 */
var drawText = function (ctx, t, x, y, w, s){
    var char = t.split('');// 文字数组
    var temp = '';// 每行临时变量
    var row = [];// 行文字数组
    var max_row = 2;

    for (var a = 0; a < char.length; a++)
    {
        if (ctx.measureText(temp).width >= w)
        {
            row.push(temp);
            temp = '';
        }
        temp += char[a];
    }
    row.push(temp);

    for (var b = 0; b < row.length; b++)
    {
        if (b >= max_row) break;
        if (b == (max_row - 1))
        {
            if (row[b+1] != undefined)
            {
                var _suffix = '...';
                var _suffix_len = Math.ceil(ctx.measureText(_suffix).width);
                var _len = Math.ceil(ctx.measureText(row[b]).width) - _suffix_len;
                row[b] = row[b].substr(0, _len) + '...';
            }
        }
        ctx.fillText(row[b], x, y + (b + 1) * (s+10));
    }
};

function downloadPoster(content, fileName) { //下载base64图片
    var base64ToBlob = function(code) {
        var parts = code.split(';base64,');
        var contentType = parts[0].split(':')[1];
        var raw = window.atob(parts[1]);
        var rawLength = raw.length;
        var uInt8Array = new Uint8Array(rawLength);
        for(var i = 0; i < rawLength; ++i) {
            uInt8Array[i] = raw.charCodeAt(i);
        }
        return new Blob([uInt8Array], {
            type: contentType
        });
    };
    var aLink = document.createElement('a');
    var blob = base64ToBlob(content); //new Blob([content]);
    var evt = document.createEvent("HTMLEvents");
    evt.initEvent("click", true, true); //initEvent 不加后两个参数在FF下会报错  事件类型，是否冒泡，是否阻止浏览器的默认行为
    aLink.download = fileName;
    aLink.href = URL.createObjectURL(blob);
    aLink.click();
};