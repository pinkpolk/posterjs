/**
 * jQuery PosterGenerator v1.0.0
 * https://gitee.com/peakxin/posterjs
 * 
 * Copyright 2019-2020, Peak Xin
 * Licensed under the MIT license.
 */
;
'use strict';

(function ($) {
    // 定义PosterGenerator为命名空间的扩展
    $.Poster = {
        qrCodeDiv: null
        , bgImgParams: {}// 背景图绘制参数
        , goodsImgParams: {}// 商品图绘制参数
        , goodsTitleParams: {}// 标题绘制参数
        , goodsPriceParams: {}// 销售价格绘制参数
        , goodsMKTPriceParams: {}// 市场价格绘制参数
        , goodsMKTPriceDelParams: {}// 市场价格删除线参数
        , goodsValidParams: {}// 有效期参数参数
        , qrcodeParams: {}// 二维码绘制参数
        , setting: {}
        , settingOpt: {
            bgImgUrl: { require: true, msg: '缺少参数bgImgUrl(海报背景图地址)' },
            qrCodeUrl: { require: true, msg: '缺少参数qrCodeUrl(二维码地址)' },
            imgUrl: { require: true, msg: '缺少参数imgUrl(商品图片地址)' },
            title: { require: true, msg: '缺少参数title(商品标题)' },
            price: { require: true, msg: '缺少参数price(商品销售价格)' },
            mktPrice: { require: false, msg: '缺少参数mktPrice(商品市场价格)' },
            validText: { require: false, msg: '缺少参数validText(商品有效期文本)' },
        }
        , defaultConf: {
            poster: {
                // bt:顶部边距,bl:左侧边距,br,右侧边距,w:海报宽度,h:海报高度
                bt: 100, bl: 30, br: 30, w: 750, h: 1066
            },
            goods: {
                image: { width: 690, height: 690, bottom: 20 },
                title: {
                    font: { size: 32, weight: 'bold', family: 'Microsoft Yahei', color: '#333333', max_w: 410 },
                },
                price: {
                    font: { size: 40, weight: 'bold', family: 'Microsoft Yahei', color: '#dc1c1c' },
                },
                mkt_price: {
                    font: { size: 25, weight: 'normal', family: 'Microsoft Yahei', color: '#979797' },
                },
                valid: {
                    font: { size: 25, weight: 'normal', family: 'Microsoft Yahei', color: '#999999' },
                }
            },
            qrcode: {
                // 二维码相关参数 w:宽度,h:高度,desc:底部文字描述,desctop:底部文字描述与二维码间距
                w: 180, h: 180, desc: '长按识别二维码', desctop: 20
            }
        }
        , init: function (opt) {
            var setting = this.setting;
            this.setSetting(opt);// 给setting赋值
            var conf = this.defaultConf;// 海报基本配置参数

            this.addHtmlBox(setting);// 添加海报html元素
            this.qcCodeGenerator(setting.qrCodeUrl, conf.qrcode.w, conf.qrcode.h);// 生成二维码
            this.setParmas(setting, conf);
            //this.drawPoster();

            $(document).on('click', '.poster-box', function (e) {
                if (e.target.id != 'poster'){
                    $('.poster-box').hide();
                }
            });

            return this;
        }
        , setSetting: function (opt) {
            // 给setting赋值
            var that = this;
            $.each(that.settingOpt, function (i, val) {
                var _settingVal = '';
                if (opt[i] == undefined || opt[i] == '') {
                    if (val.require) {
                        console.error(val.msg);
                        return false;
                    }
                    that.setting[i] = '';
                }
                else {
                    _settingVal = opt[i];
                }
                eval('that.setting.' + i + ' = _settingVal');
            });
        }
        , addHtmlBox: function (setting) {
            // 添加海报相关Html
            var _posterBoxHtml = '<div class="poster-box" style="display: none;"><div class="poster-img"><img id="poster" src="' + setting.bgImgUrl + '" alt=""><div class="close" ><span>&times;</span></div></div><div class="poster-bottom"><button type="button" class="download" >长按保存海报</button></div></div>';
            $('body').append(_posterBoxHtml);
        }
        , qcCodeGenerator: function (text, width, height) {
            // 生成二维码
            this.qrCodeDiv = document.createElement('div');
            new QRCode(this.qrCodeDiv, {
                text: text,
                width: width,
                height: height
            });
        }
        , setParmas: function (setting, conf) {
            var that = this;
            that.bgImgParams = {
                x: 0, y: 0, w: conf.poster.w, h: conf.poster.h
            };
            that.goodsImgParams = {
                x: conf.poster.bl,// x轴
                y: conf.poster.bt,// y轴
                w: conf.goods.image.width,// 宽度
                h: conf.goods.image.height// 高度
            };
            that.goodsTitleParams = {
                t: setting.title,// 文字内容
                x: conf.poster.bl,// x轴
                y: that.goodsImgParams.y + that.goodsImgParams.h + conf.goods.image.bottom - 15,// y轴
                max_w: conf.goods.title.font.max_w,// 字数长度
                s: conf.goods.title.font.size,// 字体大小
                c: conf.goods.title.font.color,// 字体颜色
                f: conf.goods.title.font.weight + ' ' + conf.goods.title.font.size + 'px ' + conf.goods.title.font.family// 字体信息
            };
            that.qrcodeParams = {
                x: conf.poster.w - conf.qrcode.w - conf.poster.br,
                y: that.goodsImgParams.y + that.goodsImgParams.h + conf.goods.image.bottom,
            };
            that.qrcodeTextParams = {
                x: that.qrcodeParams.x + (conf.qrcode.w / 2),
                y: that.qrcodeParams.y + conf.qrcode.h + conf.qrcode.desctop * 2,
                c: "#2a2a2a",
                f: "24px Microsoft Yahei",
                d: conf.qrcode.desc,
            };
            that.goodsPriceParams = {
                t: setting.price,
                x: conf.poster.bl,
                y: that.qrcodeParams.y + conf.qrcode.h,
                c: conf.goods.price.font.color,
                f: conf.goods.price.font.weight + ' ' + conf.goods.price.font.size + 'px ' + conf.goods.price.font.family
            };
            that.goodsMKTPriceParams = {
                t: setting.mktPrice,
                x: that.goodsPriceParams.x + 230,
                y: that.qrcodeParams.y + that.qrcodeParams.h,
                c: conf.goods.mkt_price.font.color,
                f: conf.goods.mkt_price.font.weight + ' ' + conf.goods.mkt_price.font.size + 'px ' + conf.goods.mkt_price.font.family
            };
            that.goodsMKTPriceDelParams = {
                s: {
                    x: that.goodsMKTPriceParams.x,
                    y: that.goodsMKTPriceParams.y - (conf.goods.mkt_price.font.size * 0.3),
                },
                e: {
                    x: that.goodsMKTPriceParams.x + that.goodsMKTPriceParams.t.length * 15,
                    y: that.goodsMKTPriceParams.y - (conf.goods.mkt_price.font.size * 0.3),
                },
            };
            that.goodsValidParams = {
                t: setting.validText,
                x: conf.poster.bl,
                y: that.goodsTitleParams.y + 136,
                c: conf.goods.valid.font.color,
                f: conf.goods.valid.font.weight + ' ' + conf.goods.valid.font.size + 'px ' + conf.goods.valid.font.family
            };
        }
        , drawPoster: function () {
            // 画海报
            var that = this;
            const imgs = {
                bg_img: that.setting.bgImgUrl
                , goods_img: that.setting.imgUrl
            };
            
            that.imgsToInstances(imgs, instances => {
                /*canvas*/
                var canvas = document.createElement("canvas");
                canvas.width = that.defaultConf.poster.w;
                canvas.height = that.defaultConf.poster.h;
                var ctx = canvas.getContext("2d");
                
                // 绘制背景图
                let bgImgParams = that.bgImgParams;
                ctx.drawImage(instances['bg_img'], bgImgParams.x, bgImgParams.y, bgImgParams.w, bgImgParams.h);

                // 绘制商品图
                let goodsImgParams = that.goodsImgParams;
                ctx.drawImage(instances['goods_img'], goodsImgParams.x, goodsImgParams.y, goodsImgParams.w, goodsImgParams.h);

                // 绘制标题
                let goodsTitleParams = that.goodsTitleParams;
                if (goodsTitleParams.t != undefined) {
                    ctx.fillStyle = goodsTitleParams.c;
                    ctx.font = goodsTitleParams.f;
                    that.drawText(ctx, goodsTitleParams.t, goodsTitleParams.x, goodsTitleParams.y, goodsTitleParams.max_w, goodsTitleParams.s);
                }

                // 绘制销售价格
                let goodsPriceParams = that.goodsPriceParams;
                if (goodsPriceParams.t != undefined) {
                    ctx.fillStyle = goodsPriceParams.c;
                    ctx.font = goodsPriceParams.f;
                    ctx.fillText(goodsPriceParams.t, goodsPriceParams.x, goodsPriceParams.y);
                }

                // 绘制市场价格
                let goodsMKTPriceParams = that.goodsMKTPriceParams;
                if (goodsMKTPriceParams.t != undefined) {
                    ctx.fillStyle = goodsMKTPriceParams.c;
                    ctx.font = goodsMKTPriceParams.f;
                    ctx.fillText(goodsMKTPriceParams.t, goodsMKTPriceParams.x, goodsMKTPriceParams.y);
                    /*删除线*/
                    let goodsMKTPriceDelParams = that.goodsMKTPriceDelParams;
                    ctx.moveTo(goodsMKTPriceDelParams.s.x, goodsMKTPriceDelParams.s.y);// 设置起点状态
                    ctx.lineTo(goodsMKTPriceDelParams.e.x, goodsMKTPriceDelParams.e.y);// 设置末端状态
                    ctx.lineWidth = 2;// 设置线宽状态
                    ctx.strokeStyle = goodsMKTPriceParams.c;// 设置线的颜色状态
                    ctx.stroke();// 进行绘制
                }

                // 绘制商品有效期
                let goodsValidParams = that.goodsValidParams;
                if (goodsValidParams.t != undefined) {
                    ctx.fillStyle = goodsValidParams.c;
                    ctx.font = goodsValidParams.f;
                    ctx.fillText(goodsValidParams.t, goodsValidParams.x, goodsValidParams.y);
                }

                // 二维码和底部文字
                let qrcodeTextParams = that.qrcodeTextParams;
                ctx.fillStyle = qrcodeTextParams.c;
                ctx.font = qrcodeTextParams.f;
                ctx.textAlign = "center";
                ctx.fillText(qrcodeTextParams.d, qrcodeTextParams.x, qrcodeTextParams.y);
                let qrcodeParams = that.qrcodeParams;
                let qrcodeImg = that.qrCodeDiv.lastChild;
                ctx.drawImage(qrcodeImg, qrcodeParams.x, qrcodeParams.y, qrcodeImg.width, qrcodeImg.height);
                //console.log(canvas.toDataURL("image/png"));
                // 海报输出
                document.getElementById('poster').setAttribute('src', canvas.toDataURL("image/png"));
                $('.poster-box').show();
            });
        }
        , imgsToInstances: function (imgs, callback) {
            const len = Object.keys(imgs).length;
            const instances = {};
            let finished = 0;
            
            for (const key in imgs) {
                const image = new Image();
                image.crossOrigin = "anonymous";
                image.src = imgs[key];
                image.onload = () => {
                    // 图片实例化成功后存起来
                    instances[key] = image;
                    finished++;
                    if (finished === len) {
                        instances['qrcode'] = this.qrCodeDiv.lastChild
                        callback(instances);
                    }
                }
            }
        }
        , drawText: function (ctx, t, x, y, w, s) {
            let char = t.split('');// 文字数组
            let temp = '';// 每行临时变量
            let row = [];// 行文字数组
            let max_row = 2;

            for (let a = 0; a < char.length; a++) {
                if (ctx.measureText(temp).width >= w) {
                    row.push(temp);
                    temp = '';
                }
                temp += char[a];
            }
            row.push(temp);

            for (let b = 0; b < row.length; b++) {
                if (b >= max_row) break;
                if (b == (max_row - 1)) {
                    if (row[b + 1] != undefined) {
                        let _suffix = '...';
                        let _suffix_len = Math.ceil(ctx.measureText(_suffix).width);
                        let _len = Math.ceil(ctx.measureText(row[b]).width) - _suffix_len;
                        row[b] = row[b].substr(0, _len) + '...';
                    }
                }
                ctx.fillText(row[b], x, y + (b + 1) * (s + 10));
            }
        }
    };
})(jQuery);